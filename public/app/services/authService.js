angular.module('authService', [])

.factory('Auth', function($http, $q, AuthToken) {
    
    //create the factory object
    var authFactory = {};
    
    // log a user in
    authFactory.login = function(username, password) {
        return $http.post('/api/authenticate', {
            username: username,
            password: password
        })
        .success(function(data) {
            AuthToken.setToken(data.token);
            return data;
        });
    };
    
    // log a user out
    authFactory.logout = function() {
        // clear the token
        AuthToken.setToken();
    };
    
    // check if a user is logged in
    // checkk if there is a local token
    authFactory.isLoggedIn = function() {
        if(AuthToken.getToken())
            return true;
        else
            return false;
    };
    
    authFactory.getUser = function() {
        if (AuthToken.getToken())
            return $http.get('/api/me', { cache: true });
        else
            return $q.reject({message: 'User ha no token.'});
        
    };
    
    // return the factory object
    return authFactory;
})

// factory for  handling tokens
.factory('AuthToken', function($window) {
    
    // create the factory object
    var authTokenFactory = {}
    
    // get the token out of local storage
    authTokenFactory.getToken = function() {
        return $window.localStorage.getItem('token');
    };
    
    // function to clear or set token
    authTokenFactory.setToken = function(token) {
        if (token)
            $window.localStorage.setItem('token', token);
        else
            $window.localStorage.removeItem('token');
    };
    
    return authTokenFactory;
})

// aoolication configuration to integrate token into requests
.factory('AuthInterceptor', function($q, $location, AuthToken) {
    
    // create the factory object
    var interceptorFactory = {};
    
    // this will happend on al HTTP requests
    interceptorFactory.request = function(config) {
        
        // grab the token
        var token = AuthToken.getToken();
        
        // if the token exists, add it to the header as x-access-token
        if (token)
            config.headers['x-access-token'] = token;
            
        return config;
    };
    
    // happens on response errors
    interceptorFactory.responseError = function(response) {
        
        // if our server returs a 403 forbidden response
        if(response.status == 403) {
            AuthToken.setToken();
            $location.path('/login')
        }
        
        //return the errors from the server as promise
        return $q.reject(response);
    };
    
    return interceptorFactory;
});