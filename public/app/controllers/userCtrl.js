angular.module('userCtrl', ['userService'])

// user controller for the main page
.controller('userController', function(User) {
    
    var vm = this;
    
    // set a proccessing variable to show loading things
    vm.processing = true;
    
    //grab all the users at page load
    User.all()
        .success(function(data) {
            vm.processing = false;
            
            // bind the users that come back to vm.users
            vm.users = data;
        });
        
    // function to delete a user
    vm.deleteUser = function(id) {
        vm.processing = true;
        
        //accepts the user id as a para,meter
        User.delete(id)
            .success(function(data) {
                
                //get all users to update the table
                User.all()
                    .success(function(data) {
                        vm.processing = false;
                        vm.users = data;
                    });
            });
    };
})

// controller applied to user creation page
.controller('userCreateController', function(User) {
    
    var vm = this;
    
    // variable to hide/show elements of the view
    // differentiates between create or edit pages
    vm.type = 'create';
    
    // function to create a user
    vm.saveUser = function() {
        vm.processing = true;
        
        // clear the message
        vm.message = '';
        
        // use the create function in the userService
        User.create(vm.userData)
            .success(function(data) {
                vm.processing = false;
                
                // clear the form
                vm.userData = {}
                vm.message = data.message;
            });
    };
})

// controller applied to user edit page
.controller('userEditController', function($routeParams, User) {
    var vm = this;
    
    // variable to hide/show elemtes of view
    // differentiates between create or edit pages
    vm.type = 'edit';
    
    // get the user data for the user you want to edit
    User.get($routeParams.user_id)
        .success(function(data) {
            vm.userData = data;
        });
        
    // function to save the user
    vm.saveUser = function()  {
        vm.processing = true;
        vm.message = '';
        
        //call the userService function to update
        User.update($routeParams.user_id, vm.userData)
            .success(function(data) {
                vm.processing = false;
                
                //clear the form
                vm.userData = {};
                
                // bind the message from our API to vm.message
                vm.message = data.message;
            });
    };
})