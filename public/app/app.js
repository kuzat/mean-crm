angular.module('userApp', ['ngAnimate', 'app.routes','authService','mainCtrl','userCtrl','userService'])

// application configuration to integrate token into requests
.config(function($httpProvider) {
    
    // attach out auth interceptor to the http requests
    $httpProvider.interceptors.push('AuthInterceptor');
});