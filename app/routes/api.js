var User     = require("../models/user");
var jwt      = require("jsonwebtoken");
var config   = require("../../config");

module.exports = function(app, express) {
    
    var apiRoutes = express.Router();

// route for authenticating users
apiRoutes.post('/authenticate', function(req, res) {
   
   // find the user
   // select the name username and password explictly
   User.findOne({
       username: req.body.username
   }).select('name username password').exec(function(err, user) {
       if (err) throw err;
       
       // no user with that username was found
       if (!user) {
           res.json({
               success: false,
               message: 'Authentication failed. User not found.'
           });
       } else if (user) {
           
           // check of password matches
           var validPassword = user.comparePassword(req.body.password);
           if (!validPassword) {
               res.json({
                   success: false,
                   message: 'Authentication failed. Wrong password.'
               });
           } else {
               // if user is found and password is right. create a token
               var token = jwt.sign({
                  name: user.name,
                  username: user.username
               }, config.secret, {
                   expiresInMinutes: 1440 //expires in 24 hours
               });
               
               // return the information including token as JSON
               res.json({
                   success: true,
                   message: 'Enjoy yout token!',
                   token: token
               });
           }
       }
   });
});

//middleware to use for all requests
apiRoutes.use(function(req, res, next) {
    
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    
    //decode token
    if (token) {
        
        //verifies secret and checks exp
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) {
                return res.status(403).send({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                
                next()
            }
        });
    } else {
        // if there is no token
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});

// test route to make sure everything is working
apiRoutes.get('/', function(req, res) {
   res.json({ message: "hooray! welcome to our api!" }); 
});

// more routes for our API will happend here

// api endpoint to get user information
apiRoutes.get('/me', function(req, res) {
    res.send(req.decoded);
});

// on routes that end with /users
//--------------------------------
apiRoutes.route('/users')

    // create a user (accessed with POST)
    .post(function(req, res) {
        
        // create a new instance of the User model
        var user = User()
        
        // set the users information 
        user.name = req.body.name;
        user.username = req.body.username;
        user.password = req.body.password;
        
        //save the user and check for errors
        user.save(function(err) {
            if (err) {
                //duplicatee entry
                if (err.code == 11000)
                    return res.json({ success: false, message: 'A user with that username already exists. '});
                else
                    return res.send(err);
            }
            
            res.json({ message: 'User created!' });
        });
    })
    
    // get all the users (accessed with GET)
    .get(function(req, res) {
        User.find(function(err, users) {
            if (err) return res.send(err);
            
            //retrun the users
            res.json(users);
        });
    });
    
// on routes that end in /users/:user_id
// -------------------------------
apiRoutes.route('/users/:user_id')

    // get the user with that id
    .get(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            if (err) return res.send(err);
            
            //return that user
            res.json(user)
        });
    })
    
    // update the user with this id
    .put(function(req, res) {
        
        // use our user model to find the user we want
        User.findById(req.params.user_id, function(err, user) {
            if (err) return res.send(err);
            
            //update the users info only if its new
            if (req.body.name) user.name = req.body.name;
            if (req.body.username) user.username = req.body.username;
            if (req.body.password) user.password = req.body.password;
            
            //save the user
            user.save(function(err) {
                if (err) return res.send(err);
                
                //return a message
                res.json({ message: "User updated!" });
            });
        });
    })
    
    // delete the user with this id
    .delete(function(req, res) {
        User.remove({
            _id: req.params.user_id
        }, function(err, user) {
            if (err) return res.send(err);
            
            res.json({ message: "Successfully deleted" });
        });
    });
    
    return apiRoutes;
};